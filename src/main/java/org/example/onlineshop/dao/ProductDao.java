package org.example.onlineshop.dao;

import java.util.List;

import org.example.onlineshop.model.Product;

public interface ProductDao { // Pt database operations.

	public void addProduct(Product product);

	public Product getProductById(int productId);

	public List<Product> getAllProducts();

	public void updateProduct(Product product);

	public void deleteProduct(Product product);

}
