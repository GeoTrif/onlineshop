package org.example.onlineshop.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.List;

import org.example.onlineapp.mapper.OrderMapper;
import org.example.onlineapp.util.ConnectionUtil;
import org.example.onlineshop.model.Order;

public class OrderDaoImpl implements OrderDao {
	private OrderMapper orderMapper = new OrderMapper();

	@Override
	public void addOrder(Order order) {
		String sql = "insert into orders(order_name,order_date,delivery_date,customer_id_fk) values (?,?,?,?)";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, order.getName());
			preparedStatement.setDate(2, (Date) order.getOrderDate());
			preparedStatement.setDate(3, (Date) order.getDeliveryDate());
			preparedStatement.setInt(4, order.getCustomer_id());

			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public Order getOrderById(int orderId) {
		String sql = "select * from orders where order_id = ?";
		Order order = null;

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, orderId);
			ResultSet resultSet = preparedStatement.executeQuery();

			// List<Order> orders = orderMapper.mapResultSetToOrders(resultSet);
			// order = orders.get(0);

			while (resultSet.next()) {
				int id = resultSet.getInt("order_id");
				String orderName = resultSet.getString("order_name");
				Date orderDate = resultSet.getDate("order_date");
				Date deliveryDate = resultSet.getDate("delivery_date");
				int customer_id = resultSet.getInt("customer_id_fk");

				System.out.println("Order id: " + id);
				System.out.println("Order name: " + orderName);
				System.out.println("Order date: " + orderDate);
				System.out.println("Delivery date: " + deliveryDate);
				System.out.println("Customer id: " + customer_id);
			}

			resultSet.close();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return order;
	}

	@Override
	public List<Order> getAllOrders() {
		String sql = "select * from orders";
		List<Order> orders = null;

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);
			// orders = orderMapper.mapResultSetToOrders(resultSet);

			while (resultSet.next()) {
				int id = resultSet.getInt("order_id");
				String orderName = resultSet.getString("order_name");
				Date orderDate = resultSet.getDate("order_date");
				Date deliveryDate = resultSet.getDate("delivery_date");
				int customer_id = resultSet.getInt("customer_id_fk");

				System.out.println("Order id: " + id);
				System.out.println("Order name: " + orderName);
				System.out.println("Order date: " + orderDate);
				System.out.println("Delivery date: " + deliveryDate);
				System.out.println("Customer id: " + customer_id);
			}

			resultSet.close();
			statement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return orders;
	}

	@Override
	public void updateOrder(Order order) {
		String sql = "update orders set order_name = ?,order_date = ?,delivery_date = ?,customer_id_fk = ? where order_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, order.getName());
			preparedStatement.setDate(2, (Date) order.getOrderDate());
			preparedStatement.setDate(3, (Date) order.getDeliveryDate());
			preparedStatement.setInt(4, order.getCustomer_id());

			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteOrder(Order order) {
		String sql = "delete from orders where order_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, order.getId());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
