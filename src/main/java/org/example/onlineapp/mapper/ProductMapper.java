package org.example.onlineapp.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.example.onlineshop.model.Product;

public class ProductMapper {
	public List<Product> mapResultSetToProducts(ResultSet resultSet) {
		List<Product> products = new ArrayList<>();

		try {
			while (resultSet.next()) {
				Product product = new Product();
				product.setId(resultSet.getInt("product_id"));
				product.setName(resultSet.getString("product_name"));
				product.setPrice(resultSet.getDouble("product_price"));
				products.add(product);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return products;
	}
}