package org.example.onlineshop.dao;

import java.util.List;

import org.example.onlineshop.model.Shop;

public interface ShopDao {

	public void addShop(Shop shop);

	public Shop getShopById(int shopId);

	public List<Shop> getAllShops();

	public void updateShop(Shop shop);

	public void deleteShop(Shop shop);

}
