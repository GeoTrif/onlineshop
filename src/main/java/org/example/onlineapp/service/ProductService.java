package org.example.onlineapp.service;

import org.example.onlineapp.util.ScannerUtil;
import org.example.onlineshop.dao.ProductDaoImpl;
import org.example.onlineshop.model.Product;

public class ProductService { // In Dao facem query,in service facem operatii pe products;apelam din dao
								// metode;
	private ProductDaoImpl productDaoImpl = new ProductDaoImpl();
	ScannerUtil scanner = new ScannerUtil();

	public void addProductService() {
		System.out.println("Enter product id:");
		int id = scanner.integerScanner();
		System.out.println("Enter product name:");
		String name = scanner.stringScanner();
		System.out.println("Enter product price:");
		double price = scanner.doubleScanner();

		Product plotter = new Product(id, name, price);
		productDaoImpl.addProduct(plotter);
	}

	public void getProductByIdService() {
		System.out.println("Enter id:");
		int id = scanner.integerScanner();
		System.out.println(productDaoImpl.getProductById(id));

	}

	public void getAllProductsService() {
		productDaoImpl.getAllProducts();
	}

	public void updateProductService() {
		System.out.println("Enter product id:");
		int id = scanner.integerScanner();
		System.out.println("Enter product name:");
		String name = scanner.stringScanner();
		System.out.println("Enter product price:");
		double price = scanner.doubleScanner();

		Product product = new Product(id, name, price);
		productDaoImpl.updateProduct(product);
	}

	public void deleteProductService() {
		System.out.println("Enter product id:");
		int id = scanner.integerScanner();

		Product product = new Product(id);
		productDaoImpl.deleteProduct(product);
	}
}
