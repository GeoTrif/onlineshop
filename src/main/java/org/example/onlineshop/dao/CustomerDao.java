package org.example.onlineshop.dao;

import java.util.List;

import org.example.onlineshop.model.Customer;
import org.example.onlineshop.model.Product;

public interface CustomerDao {

	public void addCustomer(Customer customer);

	public Customer getCustomerById(int customerId);

	public List<Customer> getAllCustomers();

	public void updateCustomer(Customer customer);

	public void deleteCustomer(Customer customer);

}
