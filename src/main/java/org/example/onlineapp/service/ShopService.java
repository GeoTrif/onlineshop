package org.example.onlineapp.service;

import org.example.onlineapp.util.ScannerUtil;
import org.example.onlineshop.dao.ShopDaoImpl;
import org.example.onlineshop.model.Shop;

public class ShopService {
	private ShopDaoImpl shopDaoImpl = new ShopDaoImpl();
	ScannerUtil scanner = new ScannerUtil();

	public void addShopService() {
		System.out.println("Enter shop name: ");
		String shopName = scanner.stringScanner();
		System.out.println("Enter shop address: ");
		String shopAddress = scanner.stringScanner();
		System.out.println("Enter shop city: ");
		String shopCity = scanner.stringScanner();

		Shop shop = new Shop(shopName, shopAddress, shopCity);
		shopDaoImpl.addShop(shop);
	}

	public void getShopByIdService() {
		System.out.println("Enter shop id:");
		int id = scanner.integerScanner();
		shopDaoImpl.getShopById(id);
	}

	public void getAllShopsService() {
		shopDaoImpl.getAllShops();
	}

	public void updateShopService() {
		System.out.println("Enter shop id: ");
		int id = scanner.integerScanner();
		System.out.println("Enter shop name: ");
		String shopName = scanner.stringScanner();
		System.out.println("Enter shop address: ");
		String shopAddress = scanner.stringScanner();
		System.out.println("Enter shop city: ");
		String shopCity = scanner.stringScanner();

		Shop shop = new Shop(id, shopName, shopAddress, shopCity);
		shopDaoImpl.updateShop(shop);
	}

	public void deleteShopService() {
		System.out.println("Enter shop id: ");
		int id = scanner.integerScanner();

		Shop shop = new Shop(id);
		shopDaoImpl.deleteShop(shop);
	}
}
