package org.example.onlineshop.model;

import java.util.Date;

public class Order {
	private int id;
	private String name;
	private Date orderDate;
	private Date DeliveryDate;
	private int customer_id;

	public Order() {
	}

	public Order(int id) {
		this.id = id;
	}

	public Order(String name, Date orderDate, Date deliveryDate, int customer_id) {
		this.name = name;
		this.orderDate = orderDate;
		DeliveryDate = deliveryDate;
		this.customer_id = customer_id;
	}

	public Order(int id, String name, Date orderDate, Date deliveryDate, int customer_id) {
		this.id = id;
		this.name = name;
		this.orderDate = orderDate;
		DeliveryDate = deliveryDate;
		this.customer_id = customer_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getDeliveryDate() {
		return DeliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		DeliveryDate = deliveryDate;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
}
