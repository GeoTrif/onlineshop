package org.example.onlineapp.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.example.onlineshop.model.Order;

public class OrderMapper {
	public List<Order> mapResultSetToOrders(ResultSet resultSet) {
		List<Order> orders = new ArrayList<>();

		try {
			while (resultSet.next()) {
				Order order = new Order();
				order.setId(resultSet.getInt("order_id"));
				order.setName(resultSet.getString("order_name"));
				order.setOrderDate(resultSet.getDate("order_date"));
				order.setDeliveryDate(resultSet.getDate("delivery_date"));
				order.setCustomer_id(resultSet.getShort("customer_id_fk"));
				orders.add(order);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return orders;
	}
}
