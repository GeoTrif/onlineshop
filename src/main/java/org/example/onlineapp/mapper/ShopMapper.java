package org.example.onlineapp.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.example.onlineshop.model.Shop;

public class ShopMapper {
	public List<Shop> mapResultSetToShop(ResultSet resultSet) {
		List<Shop> shops = new ArrayList<>();

		try {
			while (resultSet.next()) {
				Shop shop = new Shop();
				shop.setId(resultSet.getInt("shop_id"));
				shop.setName(resultSet.getString("shop_name"));
				shop.setName(resultSet.getString("shop_address"));
				shop.setName(resultSet.getString("shop_city"));
				shops.add(shop);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return shops;
	}
}
