package org.example.onlineshop;

import org.example.onlineshop.controller.OnlineShopController;

public class App {

	public static void main(String[] args) {
		OnlineShopController onlineShopController = new OnlineShopController();
		onlineShopController.mainMenuFunctionality();
	}
}
