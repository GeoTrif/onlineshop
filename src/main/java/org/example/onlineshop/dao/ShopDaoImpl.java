package org.example.onlineshop.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.example.onlineapp.mapper.ShopMapper;
import org.example.onlineapp.util.ConnectionUtil;
import org.example.onlineshop.model.Shop;

public class ShopDaoImpl implements ShopDao {
	private ShopMapper shopMapper = new ShopMapper();

	@Override
	public void addShop(Shop shop) {
		String sql = "insert into shop(shop_name,shop_address,shop_city) values (?,?,?)";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, shop.getName());
			preparedStatement.setString(2, shop.getAddress());
			preparedStatement.setString(3, shop.getCity());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Shop getShopById(int shopId) {
		String sql = "select * from shop where shop_id = ?";
		Shop shop = null;

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, shopId);
			ResultSet resultSet = preparedStatement.executeQuery();

			// List<Shop> shops = shopMapper.mapResultSetToShop(resultSet);
			// shop = shops.get(0);

			while (resultSet.next()) {
				int id = resultSet.getInt("shop_id");
				String shopName = resultSet.getString("shop_name");
				String shopAddress = resultSet.getString("shop_address");
				String shopCity = resultSet.getString("shop_city");

				System.out.println("Shop id: " + id);
				System.out.println("Shop name: " + shopName);
				System.out.println("Shop address: " + shopAddress);
				System.out.println("Shop city: " + shopCity);
			}

			resultSet.close();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return shop;
	}

	@Override
	public List<Shop> getAllShops() {
		String sql = "select * from shop";
		List<Shop> shops = null;

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);
			// shops = shopMapper.mapResultSetToShop(resultSet);

			while (resultSet.next()) {
				int id = resultSet.getInt("shop_id");
				String shopName = resultSet.getString("shop_name");
				String shopAddress = resultSet.getString("shop_address");
				String shopCity = resultSet.getString("shop_city");

				System.out.println("Shop id: " + id);
				System.out.println("Shop name: " + shopName);
				System.out.println("Shop address: " + shopAddress);
				System.out.println("Shop city: " + shopCity);
			}

			resultSet.close();
			statement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return shops;
	}

	@Override
	public void updateShop(Shop shop) {
		String sql = "update shop set shop_name = ?,shop_address = ?,shop_city = ? where shop_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, shop.getName());
			preparedStatement.setString(1, shop.getAddress());
			preparedStatement.setString(1, shop.getCity());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleteShop(Shop shop) {
		String sql = "delete from shop where shop_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, shop.getId());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
