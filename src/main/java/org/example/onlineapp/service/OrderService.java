package org.example.onlineapp.service;

import java.util.Date;
import org.example.onlineapp.util.ScannerUtil;
import org.example.onlineshop.dao.OrderDaoImpl;
import org.example.onlineshop.model.Order;

public class OrderService {
	private OrderDaoImpl orderDaoImpl = new OrderDaoImpl();
	ScannerUtil scanner = new ScannerUtil();

	public void addOrderService() {
		System.out.println("Enter order name:");
		String orderName = scanner.stringScanner();
		System.out.println("Enter order date:");
		Date orderDate = new Date();
		System.out.println(orderDate.toString());
		System.out.println("Enter delivery date:");
		Date deliveryDate = new Date();
		System.out.println(deliveryDate.toString());
		System.out.println("Enter customer id: ");
		int customerId = scanner.integerScanner();

		Order order = new Order(orderName, orderDate, deliveryDate, customerId);
		orderDaoImpl.addOrder(order);
	}

	public void getOrderByIdService() {
		System.out.println("Enter order id:");
		int id = scanner.integerScanner();
		System.out.println(orderDaoImpl.getOrderById(id));
	}

	public void getAllOrdersService() {
		orderDaoImpl.getAllOrders();
	}

	public void updateOrderService() {
		System.out.println("Enter order id:");
		int id = scanner.integerScanner();
		System.out.println("Enter order name:");
		String orderName = scanner.stringScanner();
		System.out.println("Enter order date:");
		Date orderDate = new Date();
		System.out.println(orderDate.toString());
		System.out.println("Enter delivery date:");
		Date deliveryDate = new Date();
		System.out.println(deliveryDate.toString());
		System.out.println("Enter customer id: ");
		int customerId = scanner.integerScanner();

		Order order = new Order(id, orderName, orderDate, deliveryDate, customerId);
		orderDaoImpl.updateOrder(order);
	}

	public void deleteOrderService() {
		System.out.println("Enter order id:");
		int id = scanner.integerScanner();

		Order order = new Order(id);
		orderDaoImpl.deleteOrder(order);
	}
}
