package org.example.onlineapp.util;

public class Menus {

	public void printMainMenu() {
		System.out.println();
		System.out.println("Online Shop DataBase Menu");
		System.out.println("Enter:");
		System.out.println("\t1-Products");
		System.out.println("\t2-Customers");
		System.out.println("\t3-Orders");
		System.out.println("\t4-Shops");
		System.out.println("\t5-Print Menu");
		System.out.println("\t6-Exit Application");
	}

	public void printProductsMenu() {
		System.out.println();
		System.out.println("Products Menu");
		System.out.println("Enter:");
		System.out.println("\t1-Add product");
		System.out.println("\t2-Find product by product Id");
		System.out.println("\t3-Get all products available in shop");
		System.out.println("\t4-Update product");
		System.out.println("\t5-Delete product");
		System.out.println("\t6-Back to main menu");
		System.out.println("\t7-Exit Application");

	}

	public void printCustomersMenu() {
		System.out.println();
		System.out.println("Customers Menu");
		System.out.println("Enter:");
		System.out.println("\t1-Add customer");
		System.out.println("\t2-Find customer by customer Id");
		System.out.println("\t3-Get all customers registered at the shop");
		System.out.println("\t4-Update customer");
		System.out.println("\t5-Delete customer");
		System.out.println("\t6-Back to main menu");
		System.out.println("\t7-Exit Application");
	}

	public void printOrdersMenu() {
		System.out.println();
		System.out.println("Orders Menu");
		System.out.println("Enter:");
		System.out.println("\t1-Add order");
		System.out.println("\t2-Find order by order Id");
		System.out.println("\t3-Get all orders registered at the shop");
		System.out.println("\t4-Update order");
		System.out.println("\t5-Delete order");
		System.out.println("\t6-Back to main menu");
		System.out.println("\t7-Exit Application");
	}

	public void printShopsMenu() {
		System.out.println();
		System.out.println("Shops Menu");
		System.out.println("Enter:");
		System.out.println("\t1-Add shop");
		System.out.println("\t2-Find shop by shop Id");
		System.out.println("\t3-Get all shops by shop city");
		System.out.println("\t4-Update shop");
		System.out.println("\t5-Delete shop");
		System.out.println("\t6-Back to main menu");
		System.out.println("\t7-Exit Application");
	}
}
