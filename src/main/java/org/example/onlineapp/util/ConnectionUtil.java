package org.example.onlineapp.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
	private static final String DB_URL = "jdbc:mysql://localhost:3306/online_shop?autoReconnect=true&useSSL=false";
	private static final String USER = "root";
	private static final String PASSWORD = "decebalus";
	private static Connection connection;

	private ConnectionUtil() { // singleton lazy;
	}

	public static Connection makeConnection() {

		if (connection != null) {
			return connection;
		}

		try {
			connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return connection;
	}
}
