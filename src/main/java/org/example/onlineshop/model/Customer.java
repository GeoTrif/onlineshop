package org.example.onlineshop.model;

public class Customer {
	private int id;
	private String firstName;
	private String lastName;
	private String address;
	private String city;

	public Customer() {
	}

	public Customer(int id) {
		this.id = id;
	}

	public Customer(String firstName, String lastName, String address, String city) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.city = city;
	}

	public Customer(int id, String firstName, String lastName, String address, String city) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.city = city;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}
