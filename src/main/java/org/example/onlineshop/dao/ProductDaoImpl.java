package org.example.onlineshop.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.example.onlineapp.mapper.ProductMapper;
import org.example.onlineapp.util.ConnectionUtil;
import org.example.onlineshop.model.Product;

public class ProductDaoImpl implements ProductDao {
	private ProductMapper productMapper;

	@Override
	public void addProduct(Product product) {
		String sql = "insert into products(product_name,product_price) values (?,?)";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);

			preparedStatement.setString(1, product.getName());
			preparedStatement.setDouble(2, product.getPrice());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public Product getProductById(int productId) {
		String sql = "select * from products where product_id = ?";
		Product product = null;

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, productId);
			ResultSet resultSet = preparedStatement.executeQuery();

			// List<Product> products = productMapper.mapResultSetToProducts(resultSet);
			// product = products.get(0);

			while (resultSet.next()) {
				int id = resultSet.getByte("product_id");
				String productName = resultSet.getString("product_name");
				double productPrice = resultSet.getDouble("product_price");

				System.out.println("Product id: " + id);
				System.out.println("Product name: " + productName);
				System.out.println("Product price: " + productPrice);

			}

			resultSet.close();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return product;
	}

	@Override
	public List<Product> getAllProducts() {
		String sql = "select * from products";
		List<Product> products = null;

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);
			// products = productMapper.mapResultSetToProducts(resultSet);

			while (resultSet.next()) {
				int id = resultSet.getByte("product_id");
				String productName = resultSet.getString("product_name");
				double productPrice = resultSet.getDouble("product_price");

				System.out.println("Product id: " + id);
				System.out.println("Product name: " + productName);
				System.out.println("Product price: " + productPrice);

			}

			resultSet.close();
			statement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return products;
	}

	@Override
	public void updateProduct(Product product) {
		String sql = "update products set product_name = ?,product_price = ? where product_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, product.getName());
			preparedStatement.setDouble(2, product.getPrice());
			preparedStatement.setInt(3, product.getId());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleteProduct(Product product) {
		// DELETE FROM MyGuests WHERE id=3
		String sql = "delete from products where product_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, product.getId());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
