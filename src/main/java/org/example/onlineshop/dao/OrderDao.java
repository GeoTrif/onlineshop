package org.example.onlineshop.dao;

import java.util.List;

import org.example.onlineshop.model.Customer;
import org.example.onlineshop.model.Order;

public interface OrderDao {

	public void addOrder(Order order);

	public Order getOrderById(int orderId);

	public List<Order> getAllOrders();

	public void updateOrder(Order order);

	public void deleteOrder(Order order);

}
