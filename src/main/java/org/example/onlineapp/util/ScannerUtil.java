package org.example.onlineapp.util;

import java.util.Scanner;

public class ScannerUtil {
	private Scanner input = new Scanner(System.in);

	public int integerScanner() {
		int intNumber = input.nextInt();
		return intNumber;
	}

	public String stringScanner() {
		String str = input.next();
		return str;
	}

	public double doubleScanner() {
		double doubleNumber = input.nextDouble();
		return doubleNumber;
	}
}
