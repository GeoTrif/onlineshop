package org.example.onlineapp.service;

import org.example.onlineapp.util.ScannerUtil;
import org.example.onlineshop.dao.CustomerDaoImpl;
import org.example.onlineshop.model.Customer;

public class CustomerService {
	private CustomerDaoImpl customerDaoImpl = new CustomerDaoImpl();
	ScannerUtil scanner = new ScannerUtil();

	public void addCustomerService() {
		System.out.println("Enter first name:");
		String firstName = scanner.stringScanner();
		System.out.println("Enter last name:");
		String lastName = scanner.stringScanner();
		System.out.println("Enter address:");
		String address = scanner.stringScanner();
		System.out.println("Enter city:");
		String city = scanner.stringScanner();

		Customer customer = new Customer(firstName, lastName, address, city);
		customerDaoImpl.addCustomer(customer);
	}

	public void getCustomerByIdService() {
		System.out.println("Enter id:");
		int id = scanner.integerScanner();
		System.out.println(customerDaoImpl.getCustomerById(id));
	}

	public void getAllCustomersService() {
		customerDaoImpl.getAllCustomers();
	}

	public void updateCustomerService() {
		System.out.println("Enter customer id:");
		int id = scanner.integerScanner();
		System.out.println("Enter first name:");
		String firstName = scanner.stringScanner();
		System.out.println("Enter last name:");
		String lastName = scanner.stringScanner();
		System.out.println("Enter address:");
		String address = scanner.stringScanner();
		System.out.println("Enter city:");
		String city = scanner.stringScanner();

		Customer customer = new Customer(id, firstName, lastName, address, city);
		customerDaoImpl.updateCustomer(customer);
	}

	public void deleteCustomerService() {
		System.out.println("Enter customer id:");
		int id = scanner.integerScanner();

		Customer customer = new Customer(id);
		customerDaoImpl.deleteCustomer(customer);
	}
}
