package org.example.onlineshop.controller;

import java.sql.Connection;

import org.example.onlineapp.service.CustomerService;
import org.example.onlineapp.service.OrderService;
import org.example.onlineapp.service.ProductService;
import org.example.onlineapp.service.ShopService;
import org.example.onlineapp.util.Menus;
import org.example.onlineapp.util.ScannerUtil;

public class OnlineShopController { // Afiseaza datele.
	private ScannerUtil scanner = new ScannerUtil();
	private Menus menus = new Menus();
	private boolean flag;
	private ProductService productService = new ProductService();
	private CustomerService customerService = new CustomerService();
	private OrderService orderService = new OrderService();
	private ShopService shopService = new ShopService();

	public void mainMenuFunctionality() {
		menus.printMainMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		while (flag != true) {
			switch (choice) {
			case 1:
				productsMenuFunctionality();
				break;

			case 2:
				customersMenuFunctionality();
				break;

			case 3:
				ordersMenuFunctionality();
				break;

			case 4:
				shopsMenuFunctionality();
				break;

			case 5:
				mainMenuFunctionality();
				break;

			case 6:
				System.out.println("Close application...");
				flag = true;
				break;

			default:
				System.out.println("Enter a valid choice(5 to print menu):");
				choice = scanner.integerScanner();
			}
		}
	}

	public void productsMenuFunctionality() {
		menus.printProductsMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		switch (choice) {
		case 1:
			productService.addProductService();
			productsMenuFunctionality();
			break;

		case 2:
			productService.getProductByIdService();
			productsMenuFunctionality();
			break;

		case 3:
			productService.getAllProductsService();
			productsMenuFunctionality();
			break;

		case 4:
			productService.updateProductService();
			productsMenuFunctionality();
			break;

		case 5:
			productService.deleteProductService();
			productsMenuFunctionality();
			break;

		case 6:
			mainMenuFunctionality();
			break;

		case 7:
			System.out.println("Closing application...");
			flag = true;
			break;

		default:
			System.out.println("Enter a valid choice(6 to print menu):");
			choice = scanner.integerScanner();
		}
	}

	public void customersMenuFunctionality() {
		menus.printCustomersMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		switch (choice) {
		case 1:
			customerService.addCustomerService();
			customersMenuFunctionality();
			break;

		case 2:
			customerService.getCustomerByIdService();
			customersMenuFunctionality();
			break;

		case 3:
			customerService.getAllCustomersService();
			customersMenuFunctionality();
			break;

		case 4:
			customerService.updateCustomerService();
			customersMenuFunctionality();
			break;

		case 5:
			customerService.deleteCustomerService();
			customersMenuFunctionality();
			break;

		case 6:
			mainMenuFunctionality();
			break;

		case 7:
			System.out.println("Closing application...");
			flag = true;
			break;

		default:
			System.out.println("Enter a valid choice(6 to print menu):");
			choice = scanner.integerScanner();
		}
	}

	public void ordersMenuFunctionality() {
		menus.printOrdersMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		switch (choice) {
		case 1:
			orderService.addOrderService();
			ordersMenuFunctionality();
			break;

		case 2:
			orderService.getOrderByIdService();
			ordersMenuFunctionality();
			break;

		case 3:
			orderService.getAllOrdersService();
			ordersMenuFunctionality();
			break;

		case 4:
			orderService.updateOrderService();
			ordersMenuFunctionality();
			break;

		case 5:
			orderService.deleteOrderService();
			ordersMenuFunctionality();
			break;

		case 6:
			mainMenuFunctionality();
			break;

		case 7:
			System.out.println("Closing application...");
			flag = true;
			break;

		default:
			System.out.println("Enter a valid choice(6 to print menu):");
			choice = scanner.integerScanner();
		}
	}

	public void shopsMenuFunctionality() {
		menus.printShopsMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		switch (choice) {
		case 1:
			shopService.addShopService();
			shopsMenuFunctionality();
			break;

		case 2:
			shopService.getShopByIdService();
			shopsMenuFunctionality();
			break;

		case 3:
			shopService.getAllShopsService();
			shopsMenuFunctionality();
			break;

		case 4:
			shopService.updateShopService();
			shopsMenuFunctionality();
			break;

		case 5:
			shopService.deleteShopService();
			shopsMenuFunctionality();
			break;

		case 6:
			mainMenuFunctionality();
			break;

		case 7:
			System.out.println("Closing application...");
			flag = true;
			break;

		default:
			System.out.println("Enter a valid choice(6 to print menu):");
			choice = scanner.integerScanner();
		}
	}
}
