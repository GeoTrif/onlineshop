package org.example.onlineapp.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.example.onlineshop.model.Customer;

public class CustomerMapper {
	public List<Customer> mapResultSetToCustomers(ResultSet resultSet) {
		List<Customer> customers = new ArrayList<>();

		try {
			while (resultSet.next()) {
				Customer customer = new Customer();
				customer.setId(resultSet.getInt("customer_id"));
				customer.setFirstName(resultSet.getString("last_name"));
				customer.setLastName(resultSet.getString("last_name"));
				customer.setAddress(resultSet.getString("address"));
				customer.setCity(resultSet.getString("city"));
				customers.add(customer);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return customers;
	}
}
