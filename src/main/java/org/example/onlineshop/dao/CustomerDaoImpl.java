package org.example.onlineshop.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.example.onlineapp.mapper.CustomerMapper;
import org.example.onlineapp.util.ConnectionUtil;
import org.example.onlineshop.model.Customer;
import org.example.onlineshop.model.Order;
import org.example.onlineshop.model.Product;

public class CustomerDaoImpl implements CustomerDao {
	private CustomerMapper customerMapper = new CustomerMapper();

	@Override
	public void addCustomer(Customer customer) {
		String sql = "insert into customers(first_name,last_name,address,city) values (?,?,?,?)";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);

			preparedStatement.setString(1, customer.getFirstName());
			preparedStatement.setString(2, customer.getLastName());
			preparedStatement.setString(3, customer.getAddress());
			preparedStatement.setString(4, customer.getCity());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public Customer getCustomerById(int customerId) {
		String sql = "select * from customers where customer_id = ?";
		Customer customer = null;

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, customerId);
			ResultSet resultSet = preparedStatement.executeQuery();

			// List<Customer> customers = customerMapper.mapResultSetToCustomers(resultSet);
			// customer = customers.get(0);

			while (resultSet.next()) {
				int id = resultSet.getInt("customer_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String address = resultSet.getString("address");
				String city = resultSet.getString("city");

				System.out.println("Customer id: " + id);
				System.out.println("First name: " + firstName);
				System.out.println("Last name: " + lastName);
				System.out.println("Adress: " + address);
				System.out.println("City: " + city);
			}

			resultSet.close();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return customer;
	}

	@Override
	public List<Customer> getAllCustomers() {
		String sql = "select * from customers";
		List<Customer> customers = null;

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);
			// customers = customerMapper.mapResultSetToCustomers(resultSet);

			while (resultSet.next()) {
				int id = resultSet.getInt("customer_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String address = resultSet.getString("address");
				String city = resultSet.getString("city");

				System.out.println("Customer id: " + id);
				System.out.println("First name: " + firstName);
				System.out.println("Last name: " + lastName);
				System.out.println("Adress: " + address);
				System.out.println("City: " + city);
			}

			resultSet.close();
			statement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return customers;
	}

	@Override
	public void updateCustomer(Customer customer) {
		String sql = "update customers set first_name = ?,last_name = ?,address = ?,city = ? where customer_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, customer.getFirstName());
			preparedStatement.setString(2, customer.getLastName());
			preparedStatement.setString(3, customer.getAddress());
			preparedStatement.setString(4, customer.getCity());

			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteCustomer(Customer customer) {
		String sql = "delete from customers where customer_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, customer.getId());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
